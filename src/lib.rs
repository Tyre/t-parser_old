use std::fs::read_to_string;
use ssexp::{
    parsers::{DelimitedListParser, InfixKind},
    MacroMap,
    Token,
};

pub fn parse(file: &str) -> Vec<Token> {
    let code = read_to_string(file).expect(&format!("Could not open file {}", file)[..]);
    let parse_map = MacroMap::new()
        .with_infix(':', InfixKind::List, true)
        .with_infix(',', InfixKind::Append, false)
        .with_infix('\'', InfixKind::Append, true)
        .with_lists('(', ')')
        .with_comments('#')
        .with_separating_whitespaces()
        .with_strings(
            "string".into(),
            '"',
            [
                ("\\n", "\n"),
                ("\\t", "\t"),
                ("\\r", "\r"),
                ("\\'", "\'"),
                ("\\\"", "\""),
                ("\\\\", "\\"),
                ("\\\n", " "),
            ]
            .iter()
            .map(|arg| (arg.0.into(), arg.1.into()))
            .collect(),
        );

    ssexp::parse(code.chars(), DelimitedListParser(')'), parse_map)
}

